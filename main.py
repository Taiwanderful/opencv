# line id: ilvmey
import os

import cv2
import time

from pytube import YouTube

ACCOUNT = os.getenv('CAM_ACCOUNT')
PASSWORD = os.getenv('CAM_PASSWORD')
SOURCE = os.getenv('SOURCE')

video = f'rtsp://{ACCOUNT}:{PASSWORD}@192.168.0.108:554/stream1'

SHOW_LIVEVIEW = True
SHOW_INFO_IN_TERMINAL = True
SHOW_INFO_IN_LIVEVIEW = True
YELLOW = (0, 255, 255)

def write_text(img, text):
    position = (20, 100)
    font = cv2.FONT_HERSHEY_SIMPLEX
    size = 1
    color = YELLOW
    thickness = 2
    lineType =  cv2.LINE_AA
    cv2.putText(img, text, position, font, size, color, thickness, lineType)

    return img

def detect_faces(img):
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray_img)

    return faces

def show_liveview(img, shape):
    img = cv2.resize(img, shape, interpolation=cv2.INTER_CUBIC)
    cv2.namedWindow('rtsp', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('rtsp', shape[0], shape[1])
    cv2.imshow('rtsp', img)
    cv2.waitKey(1)

def capture_from_rtsp():
    start_time = time.time()
    vid = cv2.VideoCapture(video)
    if not vid.isOpened():
        raise Exception('裝置讀取失敗')

    status, img = vid.read()
    cnt = 0
    while status:
        status, img = vid.read()

        shape = img.shape
        shape = (int(shape[1]/2), int(shape[0]/2))
        faces = detect_faces(img)
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x+w, y+h), YELLOW, 2)

        cnt = cnt + 1
        time_pass = time.time() - start_time
        info_text = "Time: {:.2f}, Frame count: {:08d}, fps: {:.2f}".format(time_pass, cnt, cnt/time_pass)

        if SHOW_INFO_IN_LIVEVIEW:
            img = write_text(img, info_text)

        if SHOW_INFO_IN_TERMINAL:
            print(f"\r{info_text}", end = "")

        img = cv2.resize(img, shape, interpolation=cv2.INTER_CUBIC)

        if SHOW_LIVEVIEW:
            show_liveview(img, shape)

filename = '林昀儒.mp4'

def capture_from_video():
    vid = cv2.VideoCapture(filename)
    while True:
        status, img = vid.read()
        if not status:
            break
        shape = img.shape
        shape = (int(shape[1]/2), int(shape[0]/2))
        show_liveview(img, shape)
        cv2.waitKey(18)


def download_video():
    url = "https://www.youtube.com/watch?v=8344YySt3Xs&ab_channel=WorldTableTennis"

    yt = YouTube(url)
    stream = yt.streams.filter(res="1080p", file_extension="mp4").first()
    stream.download(output_path="./", filename=filename)


if __name__ == '__main__':
    if SOURCE == 'rtsp':
        capture_from_rtsp()
    if SOURCE == 'youtube':
        capture_from_video()
